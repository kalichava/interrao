<?php

namespace Axioma\TranslationBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Finder\Finder;

use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\DumpException;

class TranslationRequestListener
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Yaml parser instance
     */
    protected $yaml;

    /**
     * @var Finder instance
     */
    protected $finder;

    /**
     * @var App directory path
     */
    protected $appDir;

    /**
     * @var Src directory path
     */
    protected $rootDir;

    /**
     * @array Locales collection
     */
    public $collection = array();

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->yaml      = new Parser();
        $this->finder    = new Finder();
        $this->appDir    = $container->getParameter('kernel.root_dir') . '/Resources/translations';
        $this->rootDir   = $container->getParameter('kernel.root_dir') . '/../src';
        $this->container = $container;
    }

    public function onKernelRequest()
    {
        $translationFiles = $this->getTranslationsFiles();

        if(is_array($translationFiles) && count($translationFiles)) {
            $this->collection = $this->getParsedTranslationsFiles($translationFiles);
        }
    }

    public function addTranslation($key, array $locales)
    {
        if($key && count($locales)) {
            foreach($locales as $locale => $value) {
                if(array_key_exists($locale, $this->collection)) {
                    $this->collection[$locale]['entries'][$key] = $value;

                    if(count($this->collection[$locale]['data']) > 1) {
                        $localesFilePath = array_keys($this->collection[$locale]['data']);

                        $filePath = '';
                        foreach($localesFilePath as $localeFilePath) {
                            $filePath = $localeFilePath;

                            if ( preg_match( '/messages/', $localeFilePath ) ) {
                                break;
                            }
                        }

                        $this->collection[$locale]['data'][$filePath]['entries'][$key] = $value;

                        $this->updateLocaleDataTranslations($this->collection[$locale]['data'][$filePath]);
                    }
                    else {
                        foreach($this->collection[$locale]['data'] as $filePath => $data) {
                            $this->collection[$locale]['data'][$filePath]['entries'][$key] = $value;

                            $this->updateLocaleDataTranslations($this->collection[$locale]['data'][$filePath]);
                        }
                    }
                }
            }
        }

        $this->removeCache();
    }

    public function updateTranslation($key, $locale, $value)
    {
        if(isset($key) && isset($locale) && isset($value)) {
            if(array_key_exists($locale, $this->collection)) {
                $this->collection[$locale]['entries'][$key] = $value;

                foreach($this->collection[$locale]['data'] as $filePath => $data) {
                    $this->collection[$locale]['data'][$filePath]['entries'][$key] = $value;

                    $this->updateLocaleDataTranslations($this->collection[$locale]['data'][$filePath]);
                }
            }
        }

        $this->removeCache();
    }

    public function removeTranslation($key)
    {
        if(isset($key)) {
            foreach($this->collection as $locale => $collection) {
                if(array_key_exists($key, $this->collection[$locale]['entries'])) {

                    unset($this->collection[$locale]['entries'][$key]);

                    foreach($collection['data'] as $filePath => $data) {
                        if(array_key_exists($key, $data['entries'])) {

                            unset(
                                $data['entries'][$key],
                                $this->collection[$locale]['data'][$filePath]['entries'][$key]
                            );

                            $this->updateLocaleDataTranslations($data);
                        }
                    }
                }
            }
        }

        $this->removeCache();
    }

    private function updateLocaleDataTranslations(array $translationsData)
    {
        if(array_key_exists('filename', $translationsData) &&
            array_key_exists('entries', $translationsData) &&
            count($translationsData['entries'])) {

            try {
                $dumper = new Dumper();
                $yamlDumper = $dumper->dump($translationsData['entries'], 1);

                file_put_contents($translationsData['filename'], $yamlDumper);
            }
            catch (DumpException $e) {
                throw $e;
            }
        }
    }

    private function getParsedTranslationsFiles(array $files)
    {
        $locales = array();

        foreach($files as $filePath) {
            try {
                list($name, $locale, $type) = explode('.', basename($filePath));

                if($type == 'yml') {
                    $values = $this->yaml->parse(file_get_contents($filePath));

                    $fileData = array(
                        $filePath => array(
                            'filename' => $filePath,
                            'locale' => $locale,
                            'type' => $type,
                            'entries' => $values
                        )
                    );

                    if(array_key_exists($locale, $locales)) {
                        $locales[$locale]['entries'] = array_merge($values, $locales[$locale]['entries']);
                        $locales[$locale]['data'] = array_merge($fileData, $locales[$locale]['data']);
                    }
                    else {
                        $locales[$locale] = array(
                            'entries' => $values,
                            'data' => $fileData,
                        );
                    }
                }

            }
            catch (ParseException $e) {
                throw $e;
            }
        }

        return $locales;
    }

    private function getTranslationsFiles()
    {
        $this->finder
            ->files()
            ->in($this->appDir)
            ->in($this->rootDir)
            ->exclude('config')
            ->name('*.yml');

        $this->finder->sortByName();

        if(iterator_count($this->finder)) {

            $files = array();
            foreach ($this->finder as $file) {
                $files[] = $file->getRealpath();
            }

            return $files;
        }

        return FALSE;
    }


    private function removeCache()
    {
        $rootDir = $this->container->getParameter('kernel.root_dir');
        $env = array('dev', 'prod');

        foreach ($env as $item) {

            $translationsPath = $rootDir . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $item . DIRECTORY_SEPARATOR . 'translations';

            if (file_exists($translationsPath)) {

                $dir = dir($translationsPath);

                while ($file = $dir->read()) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    unlink($translationsPath . DIRECTORY_SEPARATOR . $file);
                }

                $dir->close();
                rmdir($translationsPath);
            }
        }
    }
}