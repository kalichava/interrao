<?php

namespace Axioma\TranslationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TranslationAdmin extends Admin
{
    public function getIcon()
    {
        return 'bundles/translation/icons/translation.png';
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->remove('create');
    }

    protected $baseRouteName = 'translation';
    protected $baseRoutePattern = 'translation';

}