Axioma Translation Bundle
======================

Add common sonata interface to edit YAML translation files

Features
-------------

    - Adds fully functional section to the SonataAdmin

Installation
-------------
    1. Download TranslationBundle via composer
    2. Enable the Bundle
    3. Install assets
    4. (Optionally) Change group for sonata dashboard
    5. (Optionally) Change label for sonata dashboard


    - Add line to the block "require":
        "axioma/translation-bundle": "dev-master"

    - Add block "repositories" if not exists or add line to existing:
        "repositories":{
            "axioma/translation-bundle":{ "type":"git", "url":"git@lab.axioma.lv:symfony2/translationbundle.git" }
        },

    - Run composer install

    - Enable bundle in AppKernel.php:
        "new Axioma\TranslationBundle\TranslationBundle(),"

    - Install new assets:
        php app/console assets:publish web --symlink

    - Run translation extract command on each bundle
         php app/console bcc:trans:update --force en CoreBundle --output-format=yml

    - Add routes
            translation:
                resource: "@TranslationBundle/Resources/config/routing.yml"
                prefix:   /

    - Add parameter to your services.yml:
        axioma.translation.admin.group: 'Services'

    - Add parameter to your services.yml:
        axioma.translation.admin.label: 'Translations'