<?php

namespace Interrao\FaqBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FaqController
 * @package Interrao\FaqBundle\Controller
 */
class FaqController extends Controller
{
    /**
     * Faq
     *
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $page = $em
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'faq',
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        $items = $em
            ->getRepository('Interrao:Faq')
            ->findBy(
                array('isActive' => true),
                array('position' => 'ASC')
            );

        return $this->render(
            'InterraoFaqBundle:Faq:index.html.twig',
            array(
                'page' => $page,
                'items' => $items
            )
        );
    }
}
