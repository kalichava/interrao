<?php

namespace Interrao\FaqBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Pix\SortableBehaviorBundle\Services\PositionHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FaqAdmin extends Admin
{
    public $last_position = 0;

    /** @var ContainerInterface */
    private $container;

    /** @var PositionHandler */
    private $positionService;

    protected $baseRouteName = 'faq';
    protected $baseRoutePattern = 'faq';
    protected $classnameLabel = 'faq';

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    public function setPositionService(PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add(
                    'translations',
                    'a2lix_translations_gedmo',
                    array(
                        'required' => false,
                        'translatable_class' => 'Interrao\Entity\Faq',
                        'fields' => array(
                            'question' => array(
                                'field_type' => 'textarea'
                            ),
                            'answer' => array(
                                'field_type' => 'textarea',
                                'attr' => array('class' => 'tinymce', 'data-theme' => 'short')
                            )
                        )
                    )
                )
                ->add('isActive', null)
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('question')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->last_position = $this->positionService
            ->getLastPosition($this->getRoot()->getClass());

        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('question')
            ->add('isActive', null, array('editable' => true))
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'move' => array(
                            'template' => 'InterraoFaqBundle:Admin:_sort.html.twig'
                        )
                    )
                )
            )
        ;
    }
}