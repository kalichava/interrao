<?php

namespace Interrao\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NewsController
 * @package Interrao\NewsBundle\Controller
 */
class NewsController extends Controller
{
    /**
     * News list
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $page = $em
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'news',
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        $items = $em
            ->getRepository('Interrao:News')
            ->findBy(
                array(
                    'isActive' => true
                ),
                array(
                    'updatedAt' => 'DESC'
                )
            );

        return $this->render(
            'InterraoNewsBundle:News:index.html.twig',
            array(
                'page' => $page,
                'items' => $items
            )
        );
    }

    /**
     * News show
     *
     * @param int $id
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine();

        $page = $em
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'news',
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        $news = $this
            ->getDoctrine()
            ->getRepository('Interrao:News')
            ->find($id);

        return $this->render(
            'InterraoNewsBundle:News:show.html.twig',
            array(
                'page' => $page,
                'news' => $news
            )
        );
    }

    /**
     * News block.
     *
     * @param int $limit
     * @return Response
     */
    public function recentAction($limit = 2)
    {
        $items = $this
            ->getDoctrine()
            ->getRepository('Interrao:News')
            ->findBy(
                array(
                    'isActive' => true
                ),
                array(
                    'updatedAt' => 'DESC'
                ),
                $limit
            );

        return $this->render(
            'InterraoNewsBundle:News:_recent.html.twig',
            array(
                'items' => $items
            )
        );
    }
}
