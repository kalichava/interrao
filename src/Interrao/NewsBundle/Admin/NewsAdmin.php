<?php

namespace Interrao\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class NewsAdmin extends Admin
{
    protected $baseRouteName = 'news';
    protected $baseRoutePattern = 'news';
    protected $classnameLabel = 'news';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                array(
                    'required' => false,
                    'translatable_class' => 'Interrao\Entity\News',
                    'fields' => array(
                        'title' => array(
                            'field_type' => 'text'
                        ),
                        'lead' => array(
                            'field_type' => 'textarea',
                            'attr' => array('class' => 'tinymce', 'data-theme' => 'short')
                        ),
                        'description' => array(
                            'field_type' => 'textarea',
                            'attr' => array('class' => 'tinymce')
                        )
                    )
                )
            )
            ->add('createdAt', 'date')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('createdAt')
            ->add('isActive', null, array('editable' => true))
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    )
                )
            );
    }
}