<?php

namespace Interrao\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Constraints;

/**
 * Class QuestionType
 * @package Interrao\PageBundle\Form
 */
class QuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'fullName',
                'text',
                array(
                    'required' => true,
                    'label' => 'form.question.fullName',
                    'constraints' => array(
                        new Constraints\NotBlank(),
                    )
                )
            )
            ->add(
                'email',
                'email',
                array(
                    'required' => true,
                    'label' => 'form.question.email',
                    'constraints' => array(
                        new Constraints\Email(),
                        new Constraints\NotBlank(),
                    )
                )
            )
            ->add(
                'subject',
                'text',
                array(
                    'required' => false,
                    'label' => 'form.question.subject',
                    'attr' => array('style' => 'width:550px;')
                )
            )
            ->add(
                'question',
                'textarea',
                array(
                    'required' => true,
                    'label' => 'form.question.question',
                    'attr' => array('style' => 'width:550px;'),
                    'constraints' => array(
                        new Constraints\NotBlank()
                    )
                )
            )
            ->add('captcha', 'genemu_captcha')
            ->add('send', 'submit', array('label' => 'form.question.send'));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'question';
    }
}