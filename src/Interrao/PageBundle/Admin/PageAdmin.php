<?php

namespace Interrao\PageBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Pix\SortableBehaviorBundle\Services\PositionHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PageAdmin extends Admin
{
    public $last_position = 0;

    /** @var ContainerInterface */
    private $container;

    /** @var PositionHandler */
    private $positionService;

    protected $baseRouteName = 'page';
    protected $baseRoutePattern = 'page';
    protected $classnameLabel = 'page';

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    public function setPositionService(PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('Interrao:Page');

        $query = $em
            ->createQueryBuilder('p')
            ->select('p')
            ->from('Interrao:Page', 'p')
        ;

        $parentQuery = clone $query;
        $landingQuery = clone $query;

        $parentQuery->where('p.parent IS NULL')->orderBy('p.position', 'ASC');
        $landingQuery->where('p.parent IS NOT NULL')->orderBy('p.position', 'ASC');

        $formMapper
            ->with('General')
            ->add('slug', 'text', array('required' => false))
            ->add('parent', 'sonata_type_model', array('query' => $parentQuery, 'required' => false, 'empty_value' => '', 'btn_add' => false))
            ->add('landing', 'sonata_type_model', array('query' => $landingQuery, 'required' => false, 'empty_value' => '', 'btn_add' => false))
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                array(
                    'required' => false,
                    'translatable_class' => 'Interrao\Entity\Page',
                    'fields' => array(
                        'title' => array('field_type' => 'text'),
                        'titleFooter' => array('field_type' => 'text', 'label' => 'Title in footer'),
                        'content' => array('field_type' => 'textarea', 'attr' => array('class' => 'tinymce'))
                    )
                )
            )
            ->add('isVisibleHeader', 'checkbox', array('label' => 'Header menu visible', 'required' => false))
            ->add('isVisibleFooter', 'checkbox', array('label' => 'Footer menu visible', 'required' => false))
            ->end()
            ->with('Seo')
                ->add('seo_keywords')
                ->add('seo_description')
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('slug')
            ->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->last_position = $this->positionService
            ->getLastPosition($this->getRoot()->getClass());

        $listMapper
            ->addIdentifier('title')
            ->addIdentifier('slug')
            ->addIdentifier('parent')
            ->add('isActive', null, array('editable' => true))
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'move' => array(
                            'template' => 'InterraoPageBundle:Admin:_sort.html.twig'
                        )
                    )
                )
            )
        ;
    }
}