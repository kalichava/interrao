<?php

namespace Interrao\PageBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PageBlockAdmin extends Admin
{
    protected $baseRouteName = 'page_block';
    protected $baseRoutePattern = 'page_block';
    protected $classnameLabel = 'page_block';

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
            ->add('pages')
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                array(
                    'required' => false,
                    'translatable_class' => 'Interrao\Entity\Block',
                    'fields' => array(
                        'title' => array('field_type' => 'text'),
                        'content' => array('field_type' => 'textarea', 'attr' => array('class' => 'tinymce'))
                    )
                )
            )
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('pages')
            ->add('isActive', null, array('editable' => true))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                )
            );
    }
}