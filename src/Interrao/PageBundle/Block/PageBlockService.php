<?php

namespace Interrao\PageBundle\Block;

use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;
use Interrao\Entity\Page;

/**
 * Class PageBlockService
 * @package Interrao\PageBundle\Block
 */
class PageBlockService extends BaseBlockService
{
    /**
     * @param string $name
     * @param EngineInterface $templating
     * @param EntityManager $entityManager
     */
    public function __construct($name, EngineInterface $templating, EntityManager $entityManager)
    {
        $this->name          = $name;
        $this->templating    = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $page = null;
        $settings = $blockContext->getSettings();

        if (array_key_exists('attr', $settings)) {
            if(array_key_exists('page', $settings['attr'])) {
                $page = (int) $settings['attr']['page'];
            }
        }

        $pageBlocks = $this->entityManager
            ->getRepository('Interrao:Block')
            ->findActiveByPage($page);

        if(!$pageBlocks) {
            return new Response(null);
        }

        return $this->renderResponse(
            $blockContext->getTemplate(),
            array(
                'block' => $blockContext->getBlock(),
                'settings' => $settings,
                'pageBlocks' => $pageBlocks
            ),
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'template' => 'InterraoPageBundle::_left_block.html.twig'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $form, BlockInterface $block)
    {
        throw new \RuntimeException('Not used, this block renders an empty result if no block document can be found');
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        throw new \RuntimeException('Not used, this block renders an empty result if no block document can be found');
    }
}