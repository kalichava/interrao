<?php

namespace Interrao\PageBundle\Controller;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Constraints;
use Interrao\PageBundle\Form\QuestionType;

/**
 * Class PageController
 * @package Interrao\PageBundle\Controller
 */
class PageController extends Controller
{
    /**
     * Homepage.
     *
     * @return Response
     */
    public function homeAction()
    {
        $page = $this->getDoctrine()
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'home'
                )
            );

        return $this->render(
            'InterraoPageBundle:Page:home.html.twig',
            array(
                'page' => $page
            )
        );
    }

    /**
     * Show page by slug.
     *
     * @param string $slug
     * @throws NotFoundHttpException
     * @return Response
     */
    public function showAction($slug)
    {
        $page = $this->getDoctrine()
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => $slug,
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render(
            'InterraoPageBundle:Page:show.html.twig',
            array(
                'page' => $page
            )
        );
    }

    /**
     * Request form page.
     *
     * @param Request $request
     * @throws NotFoundHttpException
     * @return Response
     */
    public function requestFormAction(Request $request)
    {
        $page = $this->getDoctrine()
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'request-form',
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        $actionUrl = $this->generateUrl('interrao_page_request_form');

        $form = $this->createFormBuilder()
            ->setAction($actionUrl)
            ->add('companyName', 'text', array(
                'label' => 'form.request.companyName',
                'required' => true,
                'constraints' => new Constraints\NotBlank(),
            ))
            ->add('companyAddress', 'text', array(
                'label' => 'form.request.companyAddress',
                'required' => true,
                'constraints' => new Constraints\NotBlank(),
            ))
            ->add('registrationNumber', 'text', array(
                'label' => 'form.request.registrationNumber',
                'required' => true,
                'constraints' => new Constraints\NotBlank(),
            ))
            ->add('consumption', 'text', array(
                'label' => 'form.request.consumption',
                'required' => false
            ))
            ->add('contactPerson', 'text', array(
                'label' => 'form.request.contactPerson',
                'required' => true,
                'constraints' => new Constraints\NotBlank(),
            ))
            ->add('phoneNumber', 'text', array(
                'label' => 'form.request.phoneNumber',
                'required' => false
            ))
            ->add('emailAddress', 'email', array(
                'label' => 'form.request.emailAddress',
                'required' => true,
                'constraints' => array(
                    new Constraints\Email(),
                    new Constraints\NotBlank(),
                )
            ))
            ->add('automaticCounter', 'checkbox', array(
                'data' => true,
                'label' => 'form.request.automaticCounter'
            ))
            ->add('send', 'submit', array('label' => 'form.request.send'))
            ->getForm();

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $message = \Swift_Message::newInstance()
                    ->setSubject($this->get('translator')->trans('mail.request_form.subject'))
                    ->setFrom('noreply@interrao.lv')
                    ->setTo($this->container->getParameter('admin_email'))
                    ->setBody(
                        $this->renderView(
                            'InterraoPageBundle:Email:request_email.txt.twig',
                            array(
                                'form' => $form
                            )
                        ),
                        'text/html'
                    )
                ;

                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('form.request.success'));

                return $this->redirect($actionUrl);
            }
        }

        return $this->render(
            'InterraoPageBundle:Page:request_form.html.twig',
            array(
                'page' => $page,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function questionDialogAction(Request $request)
    {
        $page = $this->getDoctrine()
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'client-managers',
                    'isActive' => true
                )
            );

        if (!$page) {
            return Response::create(null);
        }

        $formHandle = $this->questionFormProcess($request);

        if($formHandle instanceof JsonResponse) {
            return $formHandle;
        }

        return $this->render(
            'InterraoPageBundle:Page:question_dialog.html.twig',
            array(
                'form' => $formHandle->createView()
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function clientManagersAction(Request $request)
    {
        $page = $this->getDoctrine()
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'client-managers',
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        $formHandle = $this->questionFormProcess($request);

        if(is_string($formHandle)) {
            return $this->redirect($formHandle);
        }

        return $this->render(
            'InterraoPageBundle:Page:client_managers.html.twig',
            array(
                'page' => $page,
                'form' => $formHandle->createView()
            )
        );
    }

    /**
     * @param Request $request
     * @return string|Form
     */
    public function questionFormProcess(Request $request)
    {
        $form = $this->createForm(new QuestionType());

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject($this->get('translator')->trans('mail.client_managers.subject'))
                    ->setFrom('noreply@interrao.lv')
                    ->setTo($this->container->getParameter('admin_email'))
                    ->setBody(
                        $this->renderView(
                            'InterraoPageBundle:Email:question_email.txt.twig',
                            array(
                                'form' => $form
                            )
                        ),
                        'text/html'
                    )
                ;

                $this->get('mailer')->send($message);

                if(!$request->isXmlHttpRequest()) {
                    $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('form.question.success'));

                    return $this->generateUrl('interrao_page_client_managers');
                }

            }

            if($request->isXmlHttpRequest()) {

                $errors = array_keys($this->getErrorMessages($form));

                return JsonResponse::create(array(
                    'errors' => $errors,
                    'isValid' => $form->isValid()
                ));
            }

        }

        return $form;
    }

    /**
     * @param Form $form
     * @return array
     */
    private function getErrorMessages(Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
