<?php

namespace Interrao\PageBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class Builder
 * @package Interrao\PageBundle\Menu
 */
class Builder extends ContainerAware
{
    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function headerMenu(FactoryInterface $factory, array $options)
    {
        $em = $this->container->get('doctrine');

        $query = $em
            ->getRepository('Interrao:Page')
            ->createQueryBuilder('p')
            ->where('p.parent IS NULL')
            ->andWhere('p.isVisibleHeader = true')
            ->orderBy('p.position', 'ASC')
        ;

        $pages = $query->getQuery()->getResult();

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'menu');

        foreach($pages as $page) {

            $slug = $page->getSlug();
            $landing = $page->getLanding();

            if(is_null($slug) && $landing) {
                if($landing->getSlug()) {
                    $slug = $landing->getSlug();
                }
            }

            if($slug) {
                $menu->addChild(
                    $page->getTitle(),
                    array(
                        'route' => 'interrao_page_show',
                        'routeParameters' => array(
                            'slug' => $slug
                        )
                    )
                );
            }

        }

        $request = $this->container->get('request');
        $requestUri = $request->getRequestUri();

        $currentPage = $em->getRepository('Interrao:Page')
            ->findOneBySlug(
                str_replace('/'. $request->getLocale() .'/', '', $request->getPathInfo())
            )
        ;

        foreach ($menu->getChildren() as $child) {
            if (($child->getUri() === $requestUri) || (($currentPage && $currentPage->getParent()) && ($currentPage->getParent()->getTitle() === $child->getName()))) {
                $child->setAttribute('class', 'current');
            }
        }

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function subPageMenu(FactoryInterface $factory, array $options)
    {
        $em = $this->container->get('doctrine');
        $pages = null;

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'submenu');

        if(array_key_exists('page', $options)) {

            $pages = $em
                ->getRepository('Interrao:Page')
                ->findBy(
                    array('parent' => (int) $options['page']),
                    array('position' => 'ASC')
                )
            ;

            if($pages) {
                foreach($pages as $page) {
                    $menu->addChild(
                        $page->getTitle(),
                        array(
                            'route' => 'interrao_page_show',
                            'routeParameters' => array(
                                'slug' => $page->getSlug()
                            )
                        )
                    );
                }
            }
        }

        if($pages) {
            $requestUri = $this->container->get('request')->getRequestUri();
            foreach ($menu->getChildren() as $child) {
                if ($child->getUri() === $requestUri) {
                    $child->setAttribute('class', 'current');
                }
            }
        }

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function footerMenu(FactoryInterface $factory, array $options)
    {
        $em = $this->container->get('doctrine');

        $query = $em
            ->getRepository('Interrao:Page')
            ->createQueryBuilder('p')
            ->where('p.slug IS NOT NULL')
            ->andWhere('p.isVisibleFooter = true')
            ->orderBy('p.position', 'ASC')
        ;

        $pages = $query->getQuery()->getResult();

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'bottom-menu clearfix');

        foreach($pages as $page) {

            $slug = $page->getSlug();
            $landing = $page->getLanding();

            if(is_null($slug) && $landing) {
                if($landing->getSlug()) {
                    $slug = $landing->getSlug();
                }
            }

            if($slug) {
                $menu->addChild(
                    $page->getTitleFooter(),
                    array(
                        'route' => 'interrao_page_show',
                        'routeParameters' => array(
                            'slug' => $slug
                        )
                    )
                );
            }

        }

        $requestUri = $this->container->get('request')->getRequestUri();
        foreach ($menu->getChildren() as $child) {
            if ($child->getUri() === $requestUri) {
                $child->setAttribute('class', 'current');
            }
        }

        return $menu;
    }
}