<?php

namespace Interrao\BannerBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Interrao\Entity\Banner;

class BannerAdmin extends Admin
{
    private $container = null;

    protected $baseRouteName = 'banner';
    protected $baseRoutePattern = 'banner';
    protected $classnameLabel = 'banner';

    public function __construct($code, $class, $baseControllerName, $container = null)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->container = $container;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $helpBanner = '';
        if ($this->getSubject()->getBanner()) {
            $bannerFile = '/uploads/assets/' . basename($this->getSubject()->getBanner());
            $helpBanner = '<img src="' . $bannerFile . '" width="500" border="0" />';
        }

        $formMapper
            ->with('General')
            ->add(
                'translations',
                'a2lix_translations_gedmo',
                array(
                    'required' => false,
                    'translatable_class' => 'Interrao\Entity\Banner',
                    'fields' => array(
                        'title' => array(
                            'field_type' => 'text'
                        ),
                        'description' => array(
                            'field_type' => 'textarea'
                        )
                    )
                )
            )
            ->add('banner', 'file', array('required' => false, 'data_class' => null, 'mapped' => true, 'help' => $helpBanner))
            ->add('spot', 'sonata_type_translatable_choice', array('required' => true, 'choices' => Banner::getSpotValues(), 'catalogue' => 'InterraoBannerBundle'))
            ->add('page', null, array('class' => 'Interrao\Entity\Page'))
            ->add('isActive')
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('spot')
            ->add('title')
            ->add('description')
            ->add('page')
            ->add('isActive', null, array('editable' => true))
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    )
                )
            );
    }

    /**
     * @param Banner $object
     * @return mixed|void
     */
    public function prePersist($object)
    {
        /** @var \Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager $uploadableManager */
        $uploadableManager = $this->container->get('stof_doctrine_extensions.uploadable.manager');
        $uploadableManager->markEntityToUpload($object, $object->getBanner());
    }

    /**
     * @param Banner $object
     * @return mixed|void
     */
    public function preUpdate($object)
    {
        /** @var \Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager $uploadableManager */
        $uploadableManager = $this->container->get('stof_doctrine_extensions.uploadable.manager');
        if (is_object($object->getBanner()) && $object->getBanner() instanceof UploadedFile) {
            $uploadableManager->markEntityToUpload($object, $object->getBanner());
        }
    }
}