<?php

namespace Interrao\BannerBundle\Block;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Interrao\Entity\Banner;

/**
 * Class BannerBlockService
 * @package Interrao\BannerBundle\Block
 */
class BannerBlockService extends BaseBlockService
{
    /**
     * @param string $name
     * @param EngineInterface $templating
     * @param EntityManager $entityManager
     */
    public function __construct($name, EngineInterface $templating, EntityManager $entityManager)
    {
        $this->name          = $name;
        $this->templating    = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $page = null;
        $spotId = Banner::SPOT_TOP;

        $settings = $blockContext->getSettings();

        if (array_key_exists('attr', $settings)) {
            if(array_key_exists('page', $settings['attr'])) {
                $page = (int) $settings['attr']['page'];
            }

            if(array_key_exists('spot', $settings['attr'])) {
                $spot = sprintf('%s_%s', 'SPOT', mb_strtoupper($settings['attr']['spot']));
                $ref = new \ReflectionClass('Interrao\Entity\Banner');
                $spotId = $ref->getConstant($spot);
            }
        }

        $template = sprintf('InterraoBannerBundle::%s.html.twig', $settings['attr']['spot']);

        $banners = $this->entityManager->getRepository('Interrao:Banner')
          ->findBy(
              array(
                  'page' => $page,
                  'spot' => $spotId,
                  'isActive' => true
              ),
              null,
              ($spotId != Banner::SPOT_CAROUSEL) ? 1 : null
          );

        return $this->renderResponse(
            $template,
            array(
                'items' => $banners,
                'block' => $blockContext->getBlock(),
                'settings' => $settings
            ),
            $response
        );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'template' => 'InterraoBannerBundle::carousel.html.twig'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $form, BlockInterface $block)
    {
        throw new \RuntimeException('Not used, this block renders an empty result if no block document can be found');
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        throw new \RuntimeException('Not used, this block renders an empty result if no block document can be found');
    }
}
