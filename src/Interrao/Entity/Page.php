<?php

namespace Interrao\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="page")
 * @Gedmo\TranslationEntity(class="Interrao\Entity\PageTranslation")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("slug")
 */
class Page
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     */
    protected $title;

    /**
     * @ORM\Column(name="title_footer", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     */
    protected $titleFooter;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     */
    protected $content;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $seo_keywords;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $seo_description;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @ORM\Column(name="is_visible_header", type="boolean")
     */
    protected $isVisibleHeader = true;

    /**
     * @ORM\Column(name="is_visible_footer", type="boolean")
     */
    protected $isVisibleFooter = true;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="landing_id", referencedColumnName="id")
     */
    protected $landing;

    /**
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\ManyToMany(targetEntity="Block")
     */
    protected $blocks;

    /**
     * @ORM\OneToMany(targetEntity="PageTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->blocks = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titleFooter
     *
     * @param string $titleFooter
     * @return Page
     */
    public function setTitleFooter($titleFooter)
    {
        $this->titleFooter = $titleFooter;

        return $this;
    }

    /**
     * Get titleFooter
     *
     * @return string
     */
    public function getTitleFooter()
    {
        return $this->titleFooter;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Page
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Page
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set isVisibleHeader
     *
     * @param boolean $isVisibleHeader
     * @return Page
     */
    public function setIsVisibleHeader($isVisibleHeader)
    {
        $this->isVisibleHeader = $isVisibleHeader;

        return $this;
    }

    /**
     * Get isVisibleHeader
     *
     * @return boolean
     */
    public function getIsVisibleHeader()
    {
        return $this->isVisibleHeader;
    }

    /**
     * Set isVisibleFooter
     *
     * @param boolean $isVisibleFooter
     * @return Page
     */
    public function setIsVisibleFooter($isVisibleFooter)
    {
        $this->isVisibleFooter = $isVisibleFooter;

        return $this;
    }

    /**
     * Get isVisibleFooter
     *
     * @return boolean
     */
    public function getIsVisibleFooter()
    {
        return $this->isVisibleFooter;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Page
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set translations
     *
     * @param ArrayCollection $translations
     * @return Page
     */
    public function setTranslations($translations)
    {
        foreach ($translations as $translation) {
            $translation->setObject($this);
        }

        $this->translations = $translations;
        return $this;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @return string
     */
    public static function getTranslationEntityClass()
    {
        return __CLASS__ . 'Translation';
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add blocks
     *
     * @param \Interrao\Entity\Block $blocks
     * @return Page
     */
    public function addBlock(\Interrao\Entity\Block $blocks)
    {
        $this->blocks[] = $blocks;
    
        return $this;
    }

    /**
     * Remove blocks
     *
     * @param \Interrao\Entity\Block $blocks
     */
    public function removeBlock(\Interrao\Entity\Block $blocks)
    {
        $this->blocks->removeElement($blocks);
    }

    /**
     * Get blocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Page
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set parent
     *
     * @param \Interrao\Entity\Page $parent
     * @return Page
     */
    public function setParent(\Interrao\Entity\Page $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Interrao\Entity\Page 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set landing
     *
     * @param Page $landing
     * @return Page
     */
    public function setLanding(\Interrao\Entity\Page $landing = null)
    {
        $this->landing = $landing;

        return $this;
    }

    /**
     * Get landing
     *
     * @return Page
     */
    public function getLanding()
    {
        return $this->landing;
    }
}