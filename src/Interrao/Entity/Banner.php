<?php

namespace Interrao\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Interrao\Entity\Repository\BannerRepository")
 * @ORM\Table(name="banner")
 * @Gedmo\TranslationEntity(class="Interrao\Entity\BannerTranslation")
 * @Gedmo\Uploadable(allowOverwrite=true, filenameGenerator="SHA1")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("page")
 */
class Banner
{
    const SPOT_TOP = 1;
    const SPOT_LEFT = 2;
    const SPOT_CAROUSEL = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint", length=1, nullable=false)
     * @Assert\NotBlank
     */
    protected $spot;

    /**
     * @ORM\OneToOne(targetEntity="Page", cascade={"persist"})
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     */
    protected $page;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\UploadableFilePath
     */
    protected $banner;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true, nullable=true)
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\OneToMany(targetEntity="BannerTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get current available spots for banner.
     *
     * @return array
     */
    public static function getSpotValues()
    {
        $refl = new \ReflectionClass(get_called_class());
        $spots = array_map(
            function ($spot) {
                return ucfirst(mb_strtolower(str_replace('SPOT_', '', $spot)));
            },
            array_flip($refl->getConstants())
        );

        return $spots;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set spot
     *
     * @param string $spot
     * @throws \InvalidArgumentException
     * @return Banner
     */
    public function setSpot($spot)
    {
        $refl = new \ReflectionClass($this);
        $spots = $refl->getConstants();

        if (!in_array($spot, $spots)) {
            throw new \InvalidArgumentException("Invalid spot");
        }

        $this->spot = $spot;

        return $this;
    }

    /**
     * Get spot
     *
     * @return int
     */
    public function getSpot()
    {
        return $this->spot;
    }

    /**
     * Set banner
     *
     * @param string $banner
     * @return Banner
     */
    public function setBanner($banner)
    {
        if ($banner == null && $this->banner !== null) {

        } else {
            $this->banner = $banner;
        }

        return $this;
    }

    /**
     * Get banner
     *
     * @return string
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Get banner filename
     *
     * @return string
     */
    public function getFileName()
    {
        return basename($this->banner);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Banner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Banner
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Banner
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set translations
     *
     * @param ArrayCollection $translations
     * @return Banner
     */
    public function setTranslations($translations)
    {
        foreach ($translations as $translation) {
            $translation->setObject($this);
        }

        $this->translations = $translations;
        return $this;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}