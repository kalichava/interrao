<?php

namespace Interrao\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class FaqRepository
 * @package Interrao\Entity\Repository
 */
class FaqRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getActive()
    {
        $qb = $this
          ->createQueryBuilder('f')
          ->where('f.isActive = true')
          ->orderBy('n.position', 'asc')
        ;

        return $qb->getQuery()->getResult();
    }

}