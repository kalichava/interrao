<?php

namespace Interrao\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class BlockRepository
 * @package Interrao\Entity\Repository
 */
class BlockRepository extends EntityRepository
{
    public function findActiveByPage($page = null)
    {
        $qb = $this
            ->createQueryBuilder('b')
            ->innerJoin('b.pages', 'p')
            ->where('b.isActive = true')
            ->andWhere('p.id = :page')
            ->setParameter('page', $page)
        ;

        return $qb->getQuery()->getResult();
    }

}