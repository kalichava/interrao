<?php

namespace Interrao\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="faq_translations",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *          "locale", "object_id", "field"
 *      })}
 * )
 */
class FaqTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Interrao\Entity\Faq", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}