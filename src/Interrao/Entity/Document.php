<?php

namespace Interrao\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="documents")
 * @Gedmo\TranslationEntity(class="Interrao\Entity\DocumentTranslation")
 * @Gedmo\Uploadable(allowOverwrite=true, filenameGenerator="SHA1")
 * @ORM\HasLifecycleCallbacks
 */
class Document
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     */
    protected $file;

    /**
     * @ORM\Column(name="file_url", type="text", nullable=true)
     */
    protected $fileUrl;

    /**
     * @ORM\Column(name="position", type="integer")
     * @Gedmo\SortablePosition
     */
    protected $position;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\OneToMany(targetEntity="DocumentTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Document
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Document
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Document
     */
    public function setFile($file)
    {
        if ($file == null && $this->file !== null) {

        } else {
            $this->file = $file;
        }

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get document filename
     *
     * @return string
     */
    public function getFileName()
    {
        return basename($this->file);
    }

    /**
     * Get document type
     *
     * @return string
     */
    public function getFileType()
    {
        $type = null;

        if($this->file) {
            $typeData = explode('.', $this->file);
            if(!empty($typeData)) {
                $type = mb_strtoupper(end($typeData));
            }
        } elseif($this->fileUrl) {
            $fileUrlData = parse_url($this->fileUrl);
            if(array_key_exists('host', $fileUrlData) && !empty($fileUrlData['host'])) {
                $type = $fileUrlData['host'];
            }
        }

        return $type;
    }

    /**
     * Get document size
     *
     * @return string
     */
    public function getFileSize()
    {
        $size = null;
        if($this->file && file_exists($this->file)) {
            $size = filesize($this->file);
        }

        return $size;
    }


    /**
     * Set position
     *
     * @param integer $position
     * @return Faq
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt($value = null)
    {
        $this->createdAt = ($value) ? $value : new \DateTime('now');
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return ($this->createdAt === null) ? new \DateTime('now') : $this->createdAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Document
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set translations
     *
     * @param ArrayCollection $translations
     * @return Document
     */
    public function setTranslations($translations)
    {
        foreach ($translations as $translation) {
            $translation->setObject($this);
        }

        $this->translations = $translations;
        return $this;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Set fileUrl
     *
     * @param string $fileUrl
     * @return Document
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
    
        return $this;
    }

    /**
     * Get fileUrl
     *
     * @return string 
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }
}