<?php

namespace Interrao\DocumentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $page = $em
            ->getRepository('Interrao:Page')
            ->findOneBy(
                array(
                    'slug' => 'documents',
                    'isActive' => true
                )
            );

        if (!$page) {
            throw $this->createNotFoundException('Page not found');
        }

        $documents = $em
            ->getRepository('Interrao:Document')
            ->findBy(
                array(
                    'isActive' => true
                )
            )
        ;

        return $this->render(
            'InterraoDocumentBundle:Default:index.html.twig',
            array(
                'page' => $page,
                'documents' => $documents
            )
        );
    }
}
