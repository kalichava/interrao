<?php

namespace Interrao\DocumentBundle\Admin;

use Interrao\Entity\Document;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Pix\SortableBehaviorBundle\Services\PositionHandler;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentAdmin extends Admin
{
    public $last_position = 0;

    /** @var ContainerInterface */
    private $container;

    /** @var PositionHandler */
    private $positionService;

    protected $baseRouteName = 'document';
    protected $baseRoutePattern = 'document';
    protected $classnameLabel = 'document';

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    public function setPositionService(PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $helpFile = '';
        if ($this->getSubject()->getFile()) {
            $fileUrl = '/uploads/assets/' . basename($this->getSubject()->getFile());
            $helpFile = '<a target="_blank" href="' . $fileUrl . '" />'. $this->getSubject()->getFileName() .'</a>';
        }

        $formMapper
            ->with('General')
                ->add(
                    'translations',
                    'a2lix_translations_gedmo',
                    array(
                        'required' => false,
                        'translatable_class' => 'Interrao\Entity\Document',
                        'fields' => array(
                            'title' => array(
                                'field_type' => 'text'
                            ),
                            'description' => array(
                                'field_type' => 'textarea',
                                'attr' => array('class' => 'tinymce', 'data-theme' => 'short')
                            )
                        )
                    )
                )
                ->add('file', 'file', array('required' => false, 'data_class' => null, 'mapped' => true, 'help' => $helpFile))
                ->add('fileUrl', 'textarea', array('required' => false))
                ->add('createdAt', 'date')
                ->add('isActive', null)
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->last_position = $this->positionService
            ->getLastPosition($this->getRoot()->getClass());

        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('isActive', null, array('editable' => true))
            ->add(
                '_action',
                '_action',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'move' => array(
                            'template' => 'InterraoDocumentBundle:Admin:_sort.html.twig'
                        )
                    )
                )
            )
        ;
    }

    /**
     * @param Document $object
     * @return mixed|void
     */
    public function prePersist($object)
    {
        /** @var UploadableManager $uploadableManager */
        $uploadableManager = $this->container->get('stof_doctrine_extensions.uploadable.manager');
        if (is_object($object->getFile()) && $object->getFile() instanceof UploadedFile) {
            $uploadableManager->markEntityToUpload($object, $object->getFile());
        }
    }

    /**
     * @param Document $object
     * @return mixed|void
     */
    public function preUpdate($object)
    {
        /** @var UploadableManager $uploadableManager */
        $uploadableManager = $this->container->get('stof_doctrine_extensions.uploadable.manager');
        if (is_object($object->getFile()) && $object->getFile() instanceof UploadedFile) {
            $uploadableManager->markEntityToUpload($object, $object->getFile());
        }
    }
}