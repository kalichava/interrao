function slideSwitch() {
    var $activeBanner = $('#slideshow .banner.active');
    var $activeCapiton = $('#slideshow-menu span.active');
	
    //if ( $activeBanner.length == 0 ) $activeBanner = $('#slideshow .banner:last');

    var $nextBanner =  $activeBanner.next().length ? $activeBanner.next()
        : $('#slideshow .banner:first');
    var $nextCapiton =  $activeCapiton.next().length ? $activeCapiton.next()
        : $('#slideshow-menu span:first');

    $activeBanner.addClass('last-active');
    $activeCapiton.addClass('last-active');

    $nextBanner.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 500, function() {
            $activeBanner.removeClass('active last-active');
        });

    $nextCapiton.addClass('active')
        .animate({opacity: 1.0}, 500, function() {
            $activeCapiton.removeClass('active last-active');
        });
}


/* ************************* */


$(document).ready(function() {
  $('.login-form-popup').magnificPopup({
	  type:'inline'
	  });


  $('.feedback-form-popup').magnificPopup({
	  type:'inline'
	  });
	  
	
	setInterval( "slideSwitch()", 5000 );
	
	//$('ul.faq-list li a').click.parent.toggleClass('has-answer', 'has-answer' );
	$('ul.faq-list li a').click(function() {
		$(this).parent().toggleClass('has-answer');
	});

});

