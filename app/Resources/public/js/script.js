function slideSwitch() {
    var $activeBanner = $('#slideshow .banner.active');
    var $activeCapiton = $('#slideshow-menu span.active');
	
    //if ( $activeBanner.length == 0 ) $activeBanner = $('#slideshow .banner:last');

    var $nextBanner =  $activeBanner.next().length ? $activeBanner.next()
        : $('#slideshow .banner:first');
    var $nextCapiton =  $activeCapiton.next().length ? $activeCapiton.next()
        : $('#slideshow-menu span:first');

    $activeBanner.addClass('last-active');
    $activeCapiton.addClass('last-active');

    $nextBanner.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 500, function() {
            $activeBanner.removeClass('active last-active');
        });

    $nextCapiton.addClass('active')
        .animate({opacity: 1.0}, 500, function() {
            $activeCapiton.removeClass('active last-active');
        });
}


/* ************************* */


$(document).ready(function() {

    if($('#map-canvas').length) {
        function mapInitialize() {

            var myLatlng = new google.maps.LatLng(56.976814,24.165351);

            var mapOptions = {
                zoom: 14,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            var contentString = '<div id="content">'+
                '<h1 id="firstHeading" class="firstHeading">Ropažu iela 10</h1>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            // To add the marker to the map, use the 'map' property
            var image = '/images/sun.png';
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation: google.maps.Animation.DROP,
                icon: image,
                title:""
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', mapInitialize);
    }

    $('.login-form-popup').magnificPopup({
        type: 'inline'
    });

    var feedBackPopup = $('.feedback-form-popup');
    feedBackPopup.magnificPopup({
        type: 'ajax',
        ajax: {
            settings: {
                url : feedBackPopup.attr('href'),
                type : 'GET'
            }
        }
    });

    $('#slideshow, #slideshow-menu').hover(function(){
        clearInterval(timer);
    }, function(){
        timer = setInterval( "slideSwitch()", 5000 );
    });

    //$('ul.faq-list li a').click.parent.toggleClass('has-answer', 'has-answer' );
    $('ul.faq-list li a').click(function () {
        $(this).parent().toggleClass('has-answer');
    });

});

